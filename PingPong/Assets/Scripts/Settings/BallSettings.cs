using UnityEngine;

[CreateAssetMenu(fileName = "BallSettings", menuName = "BallSettings", order = 0)]
public class BallSettings : ScriptableObject
{
    [Header("Force")]
    public float minSpeed;
    public float maxSpeed;
    [Header("Size")]
    public float minSize;
    public float maxSize;
}