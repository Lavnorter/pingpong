using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionTouch : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    void OnCollisionEnter2D(Collision2D other)
    {

        if (other.gameObject.tag == "Ball")
        {
            gameManager.AddScore(1);
        }
    }
}
