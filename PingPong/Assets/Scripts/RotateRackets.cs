using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotateRackets : MonoBehaviour
{
    [SerializeField] float rotateAngle;

    void Update()
    {
        Rotator();
    }
      
    void Rotator()
    {
        var width = 6;
        transform.localRotation = Quaternion.Euler(0, 0, transform.position.x / width * rotateAngle);
    }
}
