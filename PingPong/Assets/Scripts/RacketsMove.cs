using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RacketsMove : MonoBehaviour
{
    [SerializeField] GameManager gameManager;

    [SerializeField] Transform[] transformRackets = new Transform[2];
    [SerializeField] float speed;
    float _sideLimit = 3;

    Camera camera;
    Vector2 movingPosition;


    void Awake()
    {
        camera = Camera.main;

     //   if(transformRackets[0].GetComponent<CapsuleCollider2D>().IsTouching == )
        
    }

    void Update()
    {
        GetInput();
    }

    void GetInput()
    {
        if (Input.GetMouseButton(0))
        {
            var mousePosition = camera.ScreenToWorldPoint(Input.mousePosition);
            var pos_X = Mathf.Clamp(mousePosition.x, -_sideLimit, _sideLimit);
           
            for (int i = 0; i < transformRackets.Length; i++)
            {
                movingPosition = new Vector2(pos_X, transformRackets[i].position.y);
                transformRackets[i].position = Vector2.Lerp(transformRackets[i].position, movingPosition, speed * Time.deltaTime);
            }
        }
    }

   

}
    
