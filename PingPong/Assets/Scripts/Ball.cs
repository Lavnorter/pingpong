using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ball : MonoBehaviour
{
    float forceSpeed;
    float size;

    SpriteRenderer spriteRenderer;
    Color color;
    Rigidbody2D rigidbody2D;

    [SerializeField] BallSettings ballSettings;


    void Start()
    {
        rigidbody2D = GetComponent<Rigidbody2D>();
        RunBall();
    }

    public void RunBall()
    {
        AddSettings();
        AddForceBall();
    }

    public void StopBall()
    {
        rigidbody2D.isKinematic = true;
        rigidbody2D.velocity = Vector3.zero;
    }


    void AddSettings()
    {
        forceSpeed = Random.Range(ballSettings.minSpeed, ballSettings.maxSpeed);
        size = Random.Range(ballSettings.minSize, ballSettings.maxSize);

        transform.localScale = new Vector3(size, size, size);
    }

    void ChangeColor()
    {

    }
    
    public void AddForceBall()
    {
        rigidbody2D.isKinematic = false;
        rigidbody2D.AddForce(new Vector3(Random.Range(1, 3) == 1 ? forceSpeed : -forceSpeed, Random.Range(1, 3) == 1 ? forceSpeed : -forceSpeed, 0));
    }
}
