using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    [SerializeField] Text scoreText, bestScoreText;
    [SerializeField] GameObject Ball;
    [SerializeField] Transform respawnPoint;
    int score;

    public void AddScore(int number)
    {
        score += number;
        scoreText.text = score.ToString();
        SaveScore();
    }

    void SaveScore()
    {
        PlayerPrefs.SetInt("score", score);
    }

    public void GetBestScore()
    {
        bestScoreText.text = PlayerPrefs.GetInt("score").ToString();
    }

    public void ResetScore()
    {
        PlayerPrefs.SetInt("score", 0);
        bestScoreText.text = "0";
    }

    void RestartGame()
    {
        Ball.GetComponent<Ball>().RunBall();
        scoreText.text = "0";
        score = 0;
    }

    public void GameOver() {
        GetBestScore();
        Ball.GetComponent<Ball>().StopBall();
        Ball.transform.position = respawnPoint.position;
    }

    public void StartGame()
    {
        RestartGame();
    }
    

}
